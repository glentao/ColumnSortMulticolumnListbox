<h1>Description</h1>

The ColumnSortMulticolumnListbox is a QControl and requires the QControl Toolkit, http://bit.ly/QControlNITools.  It inherits from and extends the Multicolumn Listbox control.  It implements sorting by column.

<h1>LabVIEW Version</h1>

This code is currently published in LabVIEW 2018

<h1>Build Instructions</h1>

The <b>ColumnSortMulticolumnListbox.lvclass</b> and all its members, and the <b>Sort.lvclass</b> and all its members, can be distributed as a Class Libraries and built into the using application.

<h1>Installation Guide</h1>

The <b>ColumnSortMulticolumnListbox.lvclass</b> and all its members, and the <b>Sort.lvclass</b> and all its members, can be distributed as a Class Library and built into the using application.

The <b>ColumnSortMulticolumnListbox.lvclass</b> requires the the Unicode flag in the LabVIEW INI is enabled.  
Add the flag: <code>UseUnicode=True</code>


<h1>Execution</h1>
See documentation distributed with the QControl Toolkit.

<h1>Support</h1>
Submit Issues or Merge Requests through GitLab.