This package contains a bunch of helpful VIs that are useful in almost all applications we develop at MGI.

# VI List
### Application
| VI | Description|
|---|---|
| Center Controls in Pane | Moves the origin of the panel to center all visible items in the specified pane.|
| Enabled If | A helpful way to determine the enabled state of a control based on the value of variety of data types.|
| Get Executable Version | Returns a string with the current executable's version.|
| Gray If | A helpful way to determine the disabled state of a control based on the value of variety of data types.|
| GUIDv4 | Generates a UUIDv4 GUID. This GUID complies with RFC4122 and provides a random number based GUID with 122 bits of entropy.|
| Origin at Top Left | Places the referenced VI's front panel's origin at the top left of the pane.|
| Show Scrollbars if Needed | Shows the panel's scrollbars if needed.|
| VI Reference | Opens a reference to the Caller, Current, Top Level, or specified Level VI.|
### Array
| VI | Description|
|---|---|
| Average | This VI computes and returns the average value of the given array of numbers.|
| Delete Elements | Deletes several items from an array.|
| Enumerate Enum | Given an enum value this VI will return an array containing one element with each enum value.|
| Filter 1D Array | Removes several items from an array.|
| Index Elements | Selects multiple items from an array.|
| Natural Sort | Sorts the string array by natural order. This means that something like "T2" will come before "T10".|
| Remove Duplicates | Removes duplicate items from an array.|
| Search Array | Finds all indices of the element|
| Sort 1D Array | Sorts a 1D array and keeps track of the pointers.|
| Topological Sort | Topologically Sorts a Directed Graph. Only graphs with no circular dependancies can be sorted.|
### Error
| VI | Description|
|---|---|
| Clear Error | A smaller version of the NI supplied clear error. Also adds the ability to clear a range of errors.|
### File
| VI | Description|
|---|---|
| Append Text to File | Append "Text" to the file at "Path".|
| Create Directory Chain | This VI creates any non-existant folders in "Path".|
| Make String Filesafe | Remove characters that are not allowed in File or Folder names.|
| Non Repeating Plot Color | Generate colors that are good for a white background and distinguishable from each other.|
| Replace File Extension | This VI creates replaces the file extension on "Path In" with "New Extension."|
### Timing
| VI | Description|
|---|---|
| Milliseconds Since Last Call | This VI stores the tick count on a shift register and provides as an output the number of milliseconds since the last time this VI was called.|
| Milliseconds Since Last Reset | Returns the amount of time in milliseconds since the last time the VI was reset.|
| Wait | Waits the specified number of milliseconds. If an error is wired in the no wait is performed.|
## Contributing

See [Contributing.md](CONTRIBUTING.md) for information on how to submit pull requests. Bugs can be reported using the repositories issue tracker.

#### _This package is implemented with LabVIEW 2017_
